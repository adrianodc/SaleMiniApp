package com.uncaffeperdue;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;

public class SaleAppTest {
	final String PRODUCT_CODE = "apple";
	
	@Test
    public void testCreatedOneSellOperationOfOneProduct() {
        SaleService saleService = new SaleService(50);
        generateProducts(saleService);
        saleService.processOperation(new String[] {"sell", PRODUCT_CODE, "10"});
        Map<String, List<SaleOperation>> salesByProducts = saleService.getSales();
        boolean onlyOneTypeOfSale = salesByProducts.size() == 1;
        List<SaleOperation> operationsOfProduct = salesByProducts.entrySet().stream().filter(s -> s.getKey().equals(PRODUCT_CODE)).map(p -> p.getValue()).flatMap(List::stream).collect(Collectors.toList());
        boolean hasSaleOfProduct = operationsOfProduct.size() == 1;
        assertTrue("There should be only one operation of product apple.", onlyOneTypeOfSale && hasSaleOfProduct);
    }
	
	@Test
    public void testCreatedOneAdjustement() {
        SaleService saleService = new SaleService(50);
        generateProducts(saleService);
        saleService.processOperation(new String[] {"sell", "apple", "10"});
        saleService.processOperation(new String[] {"mult", "apple", "5"});
        List<AdjustementOperation> adjustements = saleService.getAdjustements().entrySet().stream().map(e -> e.getValue()).flatMap(List::stream).collect(Collectors.toList());
        assertTrue("There should be only one adjustement operation.", adjustements.size() == 1);
    }
	
	@Test
    public void testAdjustementReflectsOnSales() {
        SaleService saleService = new SaleService(50);
        generateProducts(saleService);
        saleService.processOperation(new String[] {"sell", "apple", "10"});
        int oldQuantity = saleService.getSales().get(PRODUCT_CODE).get(0).getQuantity();
        saleService.processOperation(new String[] {"mult", "apple", "5"});
        int newQuantity = saleService.getSales().get(PRODUCT_CODE).get(0).getQuantity();
        assertTrue("order quantity should change from 10 to 50.", oldQuantity == 10 && newQuantity == 50);

    }
	
	@Test
    public void testGroupOfSells() {
        SaleService saleService = new SaleService(50);
        generateProducts(saleService);
        saleService.processOperation(new String[] {"gsell", PRODUCT_CODE, "20", "10"});
        Map<String, List<SaleOperation>> salesByProducts = saleService.getSales();
        boolean onlyOneTypeOfSale = salesByProducts.size() == 1;
        List<SaleOperation> operationsOfProduct = salesByProducts.entrySet().stream().filter(s -> s.getKey().equals(PRODUCT_CODE)).map(p -> p.getValue()).flatMap(List::stream).collect(Collectors.toList());
        boolean hasSaleOfProduct = operationsOfProduct.size() == 20;
        assertTrue("There should be 20 operations of product apple.", onlyOneTypeOfSale && hasSaleOfProduct);
    }
	
	@Test
    public void testMaximumOperations() {
        SaleService saleService = new SaleService(50);
        generateProducts(saleService);
        for(int i = 0 ; i < 30; i++) {
        	saleService.processOperation(new String[] {"sell", PRODUCT_CODE, "10"});
        	saleService.processOperation(new String[] {"mult", "apple", String.valueOf(i)});
        }
        List<AdjustementOperation> adjustements = saleService.getAdjustements().entrySet().stream().map(e -> e.getValue()).flatMap(List::stream).collect(Collectors.toList());
        List<SaleOperation> sales = saleService.getSales().entrySet().stream().filter(s -> s.getKey().equals(PRODUCT_CODE)).map(p -> p.getValue()).flatMap(List::stream).collect(Collectors.toList());
        assertTrue("There should be not more than 50 operations processed.", adjustements.size() + sales.size() == 50);
    }
	
	@Test(expected = IndexOutOfBoundsException.class)
    public void testOperationWithMissingArguments() {
		SaleService saleService = new SaleService(50);
        generateProducts(saleService);
        saleService.processOperation(new String[] {"gsell", PRODUCT_CODE, "20"});
    }
	
	@Test(expected = IllegalArgumentException.class)
    public void testAddProductWrongArguments() {
		SaleService saleService = new SaleService(50);
		saleService.addProduct("apple", "Fresh Apple UN", -10);
    }
	
	@Test(expected = IllegalArgumentException.class)
    public void testSaleOperationWrongArguments() {
		SaleService saleService = new SaleService(50);
		saleService.addProduct("apple", "Fresh Apple UN", 10);
		saleService.processOperation(new String[] {"sell", PRODUCT_CODE, "0"});
    }
	
	@Test(expected = NumberFormatException.class)
    public void testSaleOperationQuantityNotInteger() {
		SaleService saleService = new SaleService(50);
		saleService.addProduct("apple", "Fresh Apple UN", 10);
		saleService.processOperation(new String[] {"sell", PRODUCT_CODE, "a"});
    }
    
	@Test(expected = IllegalArgumentException.class)
    public void testInvalidOperationNameException() {
		SaleService saleService = new SaleService(50);
        generateProducts(saleService);
        saleService.processOperation(new String[] {"test", PRODUCT_CODE, "10"});
        List<AdjustementOperation> adjustements = saleService.getAdjustements().entrySet().stream().map(e -> e.getValue()).flatMap(List::stream).collect(Collectors.toList());
        List<SaleOperation> sales = saleService.getSales().entrySet().stream().filter(s -> s.getKey().equals(PRODUCT_CODE)).map(p -> p.getValue()).flatMap(List::stream).collect(Collectors.toList());
        assertTrue("There should be not more than 50 operations processed.", adjustements.size() == 0 && sales.size() == 0);
	}
	
	private void generateProducts(SaleService saleService) {
		saleService.addProduct("apple", "Fresh Apple UN", 10);
		saleService.addProduct("soap", "Soap for Ordinary Clothes", 5);
		saleService.addProduct("gum", "Fancy Chewing Gum", 10);
		saleService.addProduct("toothbrush", "Toothbrush for Kids", 2);
	}
}
