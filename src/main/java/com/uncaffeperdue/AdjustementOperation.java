package com.uncaffeperdue;

public class AdjustementOperation {
	private Product product;
	private int quantity;
	private OperationType type;
	
	public AdjustementOperation(Product product, int quantity, OperationType type) {
		this.product = product;
		this.quantity = quantity;
		this.type = type;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public OperationType getType() {
		return type;
	}
}
