package com.uncaffeperdue;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaleApp {
	private final static Logger LOGGER = Logger.getLogger(SaleApp.class.getName());
	public static void main(String[] args) {
		int maxMessages = 0;
		int messagesCount = 0;
		LOGGER.setLevel(Level.INFO);
		LOGGER.setUseParentHandlers(false);
		ConsoleHandler handler = new ConsoleHandler();
		handler.setFormatter(new SaleLogFormatter());
		LOGGER.addHandler(handler);
		if(args.length == 1) {
			maxMessages = Integer.valueOf(args[0]);
		} else {
			LOGGER.severe("You should set only the maximum number of messages. E.g.: 50.");
			throw new IllegalArgumentException("You should set only the maximum number of messages. E.g.: 50.");
		}
		SaleService saleService = new SaleService(5);
		saleService.addProduct("apple", "Fresh Apple UN", 10);
		saleService.addProduct("soap", "Soap for Ordinary Clothes", 5);
		saleService.addProduct("gum", "Fancy Chewing Gum", 10);
		saleService.addProduct("toothbrush", "Toothbrush for Kids", 2);
		
	    Scanner scanner = new Scanner(System.in);
	    while(messagesCount < maxMessages) {
	    	LOGGER.info("Enter operation: ");
	        String operation = scanner.nextLine();
	        String[] split = operation.split("\\s+");
	        if(split.length == 3 || split.length == 4) {
	        	saleService.processOperation(split);
	        } else {
	        	LOGGER.info("You can only enter operations with three or four arguments. E.g.: [add 20 apple] or [sell apple 10].");
	        }
	        messagesCount++;
	    }
	    LOGGER.info("The maximum number of orders was reached. No more orders will be processed. Hit ENTER to close.");
	    scanner.nextLine();
	    scanner.close();
	}
}
