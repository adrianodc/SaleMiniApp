package com.uncaffeperdue;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaleService {
	private final int maxMessages;
	private int processedMessages = 0;
	private final Logger LOGGER = Logger.getLogger(SaleApp.class.getName());
	private Map<String, List<SaleOperation>> sales = new HashMap<>();
	private Map<String, List<AdjustementOperation>> adjustements = new HashMap<>();
	private Map<String, Product> products = new HashMap<>();
	

	public SaleService(int maxMessages) {
		this.maxMessages = maxMessages;
	}
	
	public Map<String, List<SaleOperation>> getSales() {
		return sales;
	}
	
	public Map<String, List<AdjustementOperation>> getAdjustements() {
		return adjustements;
	}

	public void processOperation(String[] split) {
		if(processedMessages == maxMessages) {
			return;
		}
		OperationType operation = OperationType.getOperation(split[0]);
		switch(operation) {
			case GSELL:
				Product gSaleProduct = findProduct(split[1]);
				if(gSaleProduct != null) {
					int count = Integer.valueOf(split[2]);
					int quantity = Integer.valueOf(split[3]);
					for(int i = 0; i < count; i++) {
						generateSaleOperation(gSaleProduct, quantity);
					}
				}
				break;
			case SELL:
				Product saleProduct = findProduct(split[1]);
				if(saleProduct != null) {
					int quantity = Integer.valueOf(split[2]);
					generateSaleOperation(saleProduct, quantity);
				}
				break;
			case ADD:
			case MULT:
			case SUB:
				Product adjustProduct = findProduct(split[1]);
				if(adjustProduct != null) {
					int quantity = Integer.valueOf(split[2]);
					generateAdjustementOperation(adjustProduct, quantity, operation);
				}
				break;
			default:
				LOGGER.warning("Invalid operation.");
				break;
					
		}
		processedMessages ++;
		generateLogs();
	}

	private void generateAdjustementOperation(Product product, int quantity, OperationType type) {
		if(product == null || quantity <= 0) {
			LOGGER.warning("You must send a valid product and quantity greater than zero.");
		}
		AdjustementOperation operation = new AdjustementOperation(product, quantity, type);
		List<AdjustementOperation> operations;
		if(adjustements.containsKey(product.getCode())) {
			operations = adjustements.get(product.getCode());
		} else {
			operations = new LinkedList<>();
		}
		operations.add(operation);
		adjustements.put(product.getCode(), operations);
		processAdjustement(operation);
	}

	private void processAdjustement(AdjustementOperation operation) {
		List<SaleOperation> filteredOperations = sales.get(operation.getProduct().getCode());
		for (SaleOperation saleOperation : filteredOperations) {
			operation.getType().adjustQuantity(saleOperation, operation.getQuantity());
		}
	}

	public void addProduct(String code, String name, double price) {
		if(code == null && name == null || price <= 0.0) {
			throw new IllegalArgumentException("All fields must be set in accordance.");
		}
		products.put(code, new Product(code, name, price));
	}
	
	private Product findProduct(String code) {
		Product product = products.get(code);
		if(product == null) {
			LOGGER.warning("Product " + code + " not found.");
		}
		return product;
	}
	
	private void generateSaleOperation(Product product, int quantity) {
		if(product == null || quantity <= 0) {
			LOGGER.log(Level.SEVERE, "You must send a valid product and quantity greater than zero.");
			throw new IllegalArgumentException();
		}
		SaleOperation saleOperation = new SaleOperation(product, quantity);
		List<SaleOperation> operations;
		if(sales.containsKey(product.getCode())) {
			operations = sales.get(product.getCode());
		} else {
			operations = new LinkedList<>();
		}
		operations.add(saleOperation);
		sales.put(product.getCode(), operations);
	}
	
	private void generateLogs() {
		if(processedMessages % 10 == 0)
			logSales();
		if(processedMessages == maxMessages)
			logAdjustements();
	}

	private void logAdjustements() {
		LOGGER.info("ADJUSTEMENTS PROCESSED UNTIL NOW:");
		LOGGER.info("-------------------------------------------------");
		for (Entry<String, List<AdjustementOperation>> entry : adjustements.entrySet()) {
			for (AdjustementOperation operation : entry.getValue()) {
				LOGGER.info("PRODUCT: " + entry.getKey());
				LOGGER.info("OPERATOR: " + operation.getType().getDescription());
				LOGGER.info("VALUE: " + operation.getQuantity());
			}
			LOGGER.info("-------------------------------------------------");
		}
	}

	private void logSales() {
		LOGGER.info("\n\nORDERS PROCESSED UNTIL NOW:");
		LOGGER.info("-------------------------------------------------");
		for (Entry<String, List<SaleOperation>> entry : sales.entrySet()) {
			Double sum = entry.getValue().stream().map(op -> (op.getQuantity() * op.getProduct().getPrice())).reduce((a, b) -> a + b).orElse(0.0);
			LOGGER.info("PRODUCT: " + entry.getKey());
			LOGGER.info("NUMBER OF OPERATIONS: " + entry.getValue().size());
			LOGGER.info("UNIT PRICE: " + sum);
			LOGGER.info("TOTAL VALUE: " + sum);
			LOGGER.info("-------------------------------------------------");
		}
	}
}
