package com.uncaffeperdue;

public enum OperationType {
	GSELL("Orders in group."),
	SELL("Individual order"),
	SUB("Subtraction") {
		@Override
		public void adjustQuantity(SaleOperation operation, int value) {
			operation.setQuantity(operation.getQuantity() - value);
		}
	},
	MULT("Multiplication") {
		@Override
		public void adjustQuantity(SaleOperation operation, int value) {
			operation.setQuantity(operation.getQuantity() * value);
		}
	},
	ADD("Addition") {
		@Override
		public void adjustQuantity(SaleOperation operation, int value) {
			operation.setQuantity(operation.getQuantity() + value);
		}
	};
	
	private String description;
	OperationType(String description){
		this.description = description;
	}
	
	public static OperationType getOperation(String operation) {
		return OperationType.valueOf(operation.trim().toUpperCase());
	}
	
	public void adjustQuantity(SaleOperation operation, int value) {
		// do nothing
	}

	public String getDescription() {
		return description;
	}
}
