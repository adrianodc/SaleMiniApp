package com.uncaffeperdue;

public class SaleOperation {
	private Product product;
	private int quantity;
	
	public SaleOperation(Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
