# SaleMiniApp
Simple project to deal with messages from terminal.


To run the application, use:

### java SaleApp 50

The "50" above can be any integer to set the maximum number of messages that will be processed.

All the interactions between the user and the application is based on commnd lines.
To add one order, you can use one of the products in the system:
* apple
* toothbrush
* gum
* soap

To generate one order of 10 apples, for example, you should type:
### sell apple 10

To generate 20 orders of 10 apples (each one), type:
### gsell apple 20 10

To alter all orders of one given product, you can multiply, add or subtract from the quantity. To do that, type:

### mult apple 10

You can use:
* mult;
* add;
* sub.

This application was written only for tests. In the real world, a more robust solution would be developed, using REST services, database and other structures.
